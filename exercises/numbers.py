import pandas as pd

# Creating the data dictionary from the provided PDF data
data = {
    "Cell Phone Number": [
        "95090504", "95090666", "99013333", "99042158", "99040143",
        "99055382", "99082521", "99096350", "99115720", "99909520",
        "99908510", "99907485", "99003722", "94990522", "95958780",
        "95958380", "95956260", "95956360"
    ],
    "Amount Due": [
        9405.00, 9405.00, 62851.48, 90532.06, 64967.40, 9405.00, 42187.20,
        58167.06, 9405.00, 9405.00, 9405.00, 9405.00, 75957.02, 28481.97,
        59642.89, 9405.00, 130692.12, 0  # The last entry is incomplete and marked as zero for now
    ]
}

# Create DataFrame
df = pd.DataFrame(data)

# Save to Excel file
file_path = "/mnt/data/Cell_Phone_Amounts.xlsx"
df.to_excel(file_path, index=False)

file_path
