from datetime import datetime, timedelta
from collections import deque


def process_products(robots_info, start_time_str):
    """Simulates robots processing products on an assembly line (refactored)."""

    # Parse robot information (using dictionary comprehension)
    robots = {
        name: {'process_time': int(time), 'available_at': None}
        for name, time in (data.split('-') for data in robots_info.split(';'))
    }

    # Parse start time
    start_time = datetime.strptime(start_time_str, '%H:%M:%S')

    # Product queue (using deque)
    product_queue = deque()

    current_time = start_time
    while True:
        # Check for available robots and assign products (using a single loop)
        for name, robot in robots.items():
            if robot['available_at'] and robot['available_at'] <= current_time:
                if product_queue:
                    product = product_queue.popleft()
                    print(
                        f"{name} - {product} [{current_time.strftime('%H:%M:%S')}]")
                    robot['available_at'] = current_time + \
                        timedelta(seconds=robot['process_time'])
                else:
                    robot['available_at'] = None

        # Get new product and add it to the queue
        product = input()
        if product == "End":
            break
        product_queue.append(product)

        current_time += timedelta(seconds=1)


# Get user input
robots_info = input("Robots (name-processTime;...): ")
start_time_str = input("Start time (hh:mm:ss): ")

# Start the simulation
process_products(robots_info, start_time_str)
