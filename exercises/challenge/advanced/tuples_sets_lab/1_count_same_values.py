'''
1.	Count Same Values
You will be given numbers separated by a space. Write a program that prints the number of occurrences of each number in the format "{number} - {count} times". The number must be formatted to the first decimal point.
Examples
Input
-2.5 4 3 -2.5 -5.5 4 3 3 -2.5 3
Output
-2.5 - 3 times
4.0 - 2 times
3.0 - 4 times
-5.5 - 1 times

Input
2 4 4 5 5 2 3 3 4 4 3 3 4 3 5 3 2 5 4 3

output

2.0 - 3 times
4.0 - 6 times
5.0 - 4 times
3.0 - 7 times

do not use function
use tuple or set
'''
numbers = tuple(map(float, input().split()))
numbers_count = {}

for number in numbers:
    if number not in numbers_count:
        numbers_count[number] = 0
    numbers_count[number] += 1

for number, count in numbers_count.items():
    print(f"{number} - {count} times")

# 2.0 - 3 times
