'''
1.	Ramen Shop
Link to Judge: https://judge.softuni.org/Contests/Compete/Index/4687#0
You will be given two sequences of integers representing bowls of ramen and customers. Your task is to find out if you can serve all the customers.
Start by taking the last bowl of ramen and the first customer. Try to serve every customer with ramen until we have no more ramen or customers left:
•	Each time the value of the ramen is equal to the value of the customer, remove them both and continue with the next bowl of ramen and the next customer.
•	Each time the value of the ramen is bigger than the value of the customer, decrease the value of that ramen with the value of that customer and remove the customer. Then try to match the same bowl of ramen (which has been decreased) with the next customer.
•	Each time the customer's value is bigger than the value of the ramen bowl, decrease the value of the customer with the value of the ramen bowl and remove the bowl. Then try to match the same customer (which has been decreased) with the next bowl of ramen.
Look at the examples provided for a better understanding of the problem.
Input
•	On the first line, you will receive integers representing the bowls of ramen, separated by a single space and a comma ", ".
•	On the second line, you will receive integers representing the customers, separated by a single space and a comma ", ".
Output
•	If all customers are served, print: "Great job! You served all the customers."
o	Print all of the left ramen bowls (if any) separated by comma and space in the format: "Bowls of ramen left: {bowls of ramen left}"
•	Otherwise, print: "Out of ramen! You didn't manage to serve all customers."
o	Print all customers left separated by comma and space in the format "Customers left: {customers left}"


'''


def ramen_shop():
    ramen = input()
    customers = input()
    ramen = ramen.split(", ")
    customers = customers.split(", ")
    while ramen and customers:
        current_ramen = int(ramen.pop())
        current_customer = int(customers.pop(0))
        if current_ramen == current_customer:
            continue
        elif current_ramen > current_customer:
            ramen.append(str(current_ramen - current_customer))
        else:
            customers.insert(0, str(current_customer - current_ramen))
    if not customers:
        if ramen == customers:
            print("Great job! You served all the customers.")
        else:
            print("Great job! You served all the customers.")
            print(f"Bowls of ramen left: {', '.join(ramen)}")
    else:
        print("Out of ramen! You didn't manage to serve all customers.")
        print(f"Customers left: {', '.join(customers)}")


ramen_shop()
