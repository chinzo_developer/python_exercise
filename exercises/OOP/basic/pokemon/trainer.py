from pokemon import Pokemon


class Trainer:
    def __init__(self, name):
        self.name = name
        self.pokemon = {}

    def add_pokemon(self, pokemon: Pokemon):
        self.pokemon[pokemon.name] = pokemon
        details = pokemon.pokemon_details()
        return f"Caught {details}"

    def release_pokemon(self, pokemon_name):
        if pokemon_name in self.pokemon.keys():
            del self.pokemon[pokemon_name]
            return f"You have released {pokemon_name}"
        else:
            return "Pokemon is not caught"

    def trainer_data(self):
        count = len(self.pokemon)
        pokemons = ""

        for key, value in self.pokemon.items():
            pokemons += f" - {Pokemon.pokemon_details(value)}\n"

        return (f"Pokemon Trainer {self.name}\n"
                f"Pokemon count {count}\n"
                f"{pokemons}")


pokemon = Pokemon("Pikachu", 90)
print(pokemon.pokemon_details())
trainer = Trainer("Ash")
print(trainer.add_pokemon(pokemon))
second_pokemon = Pokemon("Charizard", 110)
print(trainer.add_pokemon(second_pokemon))
print(trainer.add_pokemon(second_pokemon))
print(trainer.release_pokemon("Pikachu"))
print(trainer.release_pokemon("Pikachu"))
print(trainer.trainer_data())
